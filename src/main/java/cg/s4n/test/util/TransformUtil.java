package cg.s4n.test.util;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;
import org.apache.commons.io.FileUtils;
import cg.s4n.test.model.Dron;
import cg.s4n.test.model.DronMovement;
import cg.s4n.test.model.RouteFinal;

public class TransformUtil
{

   private static final Logger LOGGER = Logger.getLogger(TransformUtil.class.getName());

   public static List<Dron> getDronsFromFile(List<File> files)
   {
      List<Dron> drons = new ArrayList<>();
      files.forEach(file -> {
         try
         {
            if (file.getName().contains("in") && file.getName().endsWith(".txt"))
            {
               drons.add(
                  Dron.builder().name(file.getName().replaceAll(".txt", "").replaceAll("in", ""))
                     .routes(FileUtils.readLines(file, "UTF-8"))
                     .routesFinal(new ArrayList<>())
                     .build());
            }

         }
         catch (IOException e)
         {
            LOGGER.info("Error reading file " + file.getName());
         }
      });
      drons.sort(
         (Dron h1, Dron h2) -> h1.getName().compareTo(h2.getName()));
      return drons;
   }

   public static void calculateRoute(Dron dron)
   {
      dron.getRoutes().forEach(route -> {
         dron.getRoutesFinal().add(getRouteFinal( route.toCharArray(),dron));
      });
   }

   private static RouteFinal getRouteFinal(char[] route,Dron dron)
   {
      DronMovement dronMovement=dron.getDronMovement();

      if (dronMovement==null)
      {
         dronMovement=new DronMovement();
      }
      for (char c : route)
      {
         if (c == 'A')
         {
            dronMovement.run();
         }

         if (c == 'I')
         {
            dronMovement.leftTurn();
         }

         if (c == 'D')
         {
            dronMovement.rightTurn();
         }
      }

      dronMovement.getFinalDirection();
      dron.setDronMovement(dronMovement);

      return RouteFinal.builder()
            .x(dronMovement.getActualX())
            .y(dronMovement.getActualY())
            .dir(dronMovement.getDirection()).build();
   }

}
