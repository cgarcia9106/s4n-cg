package cg.s4n.test.util;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.List;

import org.apache.commons.io.FileUtils;
import org.apache.commons.io.filefilter.TrueFileFilter;
import cg.s4n.test.model.Dron;

public class FileUtilsCg
{

   private static String DIR = "in";
   private static String DIR_OUT = "out";

   public static List<File> getFilesFromRoute()
   {

      File dir = new File(DIR);
      return (List<File>) FileUtils
         .listFiles(dir, TrueFileFilter.INSTANCE, TrueFileFilter.INSTANCE);
   }

   public static void writeFileDron(Dron dron)
   {
      try (FileWriter writer = new FileWriter(DIR_OUT + "/out" + dron.getName() + ".txt", false);
      BufferedWriter bw = new BufferedWriter(writer))
      {

         dron.getRoutesFinal().forEach(c -> {
            try
            {
               bw.write(c.toString());
               bw.newLine();
            }
            catch (IOException e)
            {
               e.printStackTrace();
            }
         });

      }
      catch (IOException e)
      {
         System.err.format("IOException: %s%n", e);
      }
   }
}
