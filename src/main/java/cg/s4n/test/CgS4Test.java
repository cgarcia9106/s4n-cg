package cg.s4n.test;

import java.util.List;
import java.util.logging.Logger;
import cg.s4n.test.model.Dron;
import cg.s4n.test.util.FileUtilsCg;
import cg.s4n.test.util.TransformUtil;

public class CgS4Test
{

   private static final Logger LOGGER = Logger.getLogger(CgS4Test.class.getName());



   public static void main(String[] args)
   {
      LOGGER.info("---- Su corrientazo a domicilio Entregas diarias----");

      List<Dron>drons=TransformUtil.getDronsFromFile(FileUtilsCg.getFilesFromRoute());
      LOGGER.info("Drons de la ruta del dia "+ drons);
      drons.forEach(dron->{
         TransformUtil.calculateRoute(dron);
         LOGGER.info("Dron " + dron.getName() +" Calculado "+dron.getRoutesFinal());
         FileUtilsCg.writeFileDron(dron);
      });
   }

}
