package cg.s4n.test.model;

import java.util.List;
import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class Dron
{
   private String name;

   private List<String>routes;

   private List<RouteFinal> routesFinal;

   private DronMovement dronMovement ;

}
