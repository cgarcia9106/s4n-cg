package cg.s4n.test.model;

import lombok.Builder;
import lombok.Getter;

@Getter
@Builder
public class RouteFinal
{

   private Integer x;
   private Integer y;
   private String dir;

   @Override
   public String toString()
   {
      String direction="";

      switch (dir)
      {
         case "N":
            direction = "Norte";
            break;
         case "S":
            direction = "Sur";
            break;

         case "Or":
            direction = "Oriente";
            break;
         case "Oc":
            direction = "Occidente";
            break;
      }
      return "(" + x + "," + y + ") " + "dirección " + direction;
   }

}
