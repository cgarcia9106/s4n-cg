package cg.s4n.test.model;

import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class DronMovement
{

   private Integer actualX = 0;
   private Integer actualY = 0;
   private String direction = "N";
   private String directionX = "";
   private String directionY = "";

   public void rightTurn()
   {
      switch (direction)
      {
         case "N":
            direction = "Or";
            break;
         case "S":
            direction = "Oc";
            break;

         case "Or":
            direction = "S";
            break;
         case "Oc":
            direction = "N";
            break;
      }
   }

   public void leftTurn()
   {
      switch (direction)
      {
         case "N":
            direction = "Oc";
            break;
         case "S":
            direction = "Or";
            break;

         case "Or":
            direction = "N";
            break;
         case "Oc":
            direction = "S";
            break;
      }
   }

   public void run(){
      switch (direction)
      {
         case "N":
            actualY=actualY+1;
            directionY="N";
            break;
         case "S":
            actualY = actualY-1;
            directionY="S";

            break;

         case "Or":
            actualX =actualX+1;
            directionX="Or";

         case "Oc":
            actualX = actualX-1;
            directionX="Oc";

            break;
      }
   }

   public  void getFinalDirection() {
      if(Math.abs(actualX)>Math.abs(actualY)) {
        direction=directionX;
      }
      if(Math.abs(actualY)>Math.abs(actualX)) {
         direction=directionY;
       }
      if(Math.abs(actualY)==Math.abs(actualX)) {
         direction=directionY;
       }
   }
}
